define(function (require) {
    'use strict';

    const $ = require('jquery');
    const _ = require('underscore');
    const __ = require('orotranslation/js/translator');
    const template = require('tpl-loader!xngageproductcompare/templates/datagrid/product-compare-popup.html');
    const BaseView = require('oroui/js/app/views/base/view');
    const mediator = require('oroui/js/mediator');
    const { productCompareInstance } = require('xngageproductcompare/js/app/models/product-compare-model')
    require('slick');

    /**
     * Renders a checkbox for row selection.
     *
     * @class   oro.datagrid.cell.BaseView
     * @extends BaseView
     */
    const BackendSelectRowCell = BaseView.extend({
        /** @property */
        autoRender: true,

        /** @property */
        sliderOptions: {
            useTransform: false, // transform in slick-slider breaks dropdowns
            mobileEnabled: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            arrows: true,
            dots: false,
            infinite: false,
            additionalClass: 'embedded-list__slider no-transform',
            embeddedArrowsClass: 'embedded-arrows',
            loadingClass: 'loading',
            itemLinkSelector: null,
            processClick: null,
            rtl: _.isRTL()
        },
        /** @property */
        className: 'popup-compare hidden',

        /** @property */
        tagName: 'div',

        /** @property */
        template: template,

        /** @property */
        secModel: productCompareInstance,

        /** @property */
        path: '',

        /** @property */
        compareButton: '[data-role="compare-button"]',

        /** @property */
        events: {
            'click [data-role="select-icon-dropdown-wrapper"]': 'handleDropdownIconClick',
            'click [data-role="remove-all-selected-compare-products"]': 'handleRemoveAllClick',
            'click [data-role="remove-compare-item-cell"]': 'handleRemoveItemClick',
            'click [data-role="compare-button"]': 'handleCompareClick',

        },
        /** @property */
        text: __('oro.product.grid.select_product'),

        /**
         * @inheritDoc
         */
        constructor: function BackendSelectRowCell(options) {
            BackendSelectRowCell.__super__.constructor.call(this, options);
        },

        /**
         * @inheritDoc
         */
        initialize: function (options) {

            if (options.productModel) {
                this.model = options.productModel;
            }
            this.path = options.path

            this.$container = $(options._sourceElement);
            this.template = this.getTemplateFunction();

            mediator.subscribe('addOrRemoveToPopupCompare', this.render, this);
            mediator.subscribe('removeallproducts', this.render, this);

        },

        /**
         * @inheritDoc
         */
        render: function () {
            const visibleState = {};
            let hide = !_.isMobile();
            let state = { selected: false }

            if (!_.isEmpty(visibleState)) {
                hide = visibleState.visible;
            }

            let ProdcutCompareArray = this.secModel.get('products');

            if (ProdcutCompareArray.length) {
                this.hideView(true)
            }


            this.$el.html(this.template({
                text: this.text,
                products: ProdcutCompareArray,
                path: this.path
            }));
            setTimeout(() => {
                let options = this.sliderOptions
                if ($(window).width() <= 810) {
                    options.slidesToShow = 2
                    $('.carouselWrapper').slick(options);

                } else if ($(window).width() <= 1149) {

                    $('.carouselWrapper').slick(options);

                }
                else if ($(window).width() <= 1330) {
                    options.slidesToShow = 5

                    $('.carouselWrapper').slick(options);

                }
            }, 0)

            this.$checkbox = this.$el.find(this.checkboxSelector);
            this.$container.append(this.$el);

            if (ProdcutCompareArray && $(this.compareButton).attr('class').split(/\s+/).includes('disabled')) {
                $(this.compareButton).removeClass('disabled');

            }
            return this;
        },

        /**
         * @param {Boolean} bool
         */
        hideView: function (bool) {
            this.$el.toggleClass('hidden', !bool);
        },

        /**
         * When the checkbox's value changes, this method will trigger a Backbone
         * `backgrid:selected` event with a reference of the model and the
         * checkbox's `checked` value.
         */
        handleDropdownIconClick: function (e) {
            if ($('.popup-compare.dropdown-hidden').length) {
                $('.popup-compare').removeClass('dropdown-hidden')
                $('.popup-compare .wrapper').removeClass('rotate-dropdown-arrow')

            } else {

                $('.popup-compare').addClass('dropdown-hidden')
                $('.popup-compare .wrapper').addClass('rotate-dropdown-arrow')
            }
        },
        handleRemoveAllClick: function (e) {
            mediator.publish('removeallproducts')

            $(this.compareButton).addClass('disabled');
            $('.popup-compare').addClass('hidden')

        },
        handleCompareClick: function (e) {
            localStorage.setItem('productCompareBack', JSON.stringify(document.location.pathname));
        },
        handleRemoveItemClick: function (e) {
            const id = e.currentTarget?.attributes["aria-owns"]?.nodeValue
            let ProdcutCompareArray = this.secModel.get('products');

            ProdcutCompareArray = ProdcutCompareArray.filter(element => {
                if (Number(element.id) === Number(id)) {
                    return false
                } else {
                    return true
                }
            })

            mediator.publish('addOrRemoveToPopupCompare', ProdcutCompareArray);

            if (!ProdcutCompareArray.length) {
                $(this.compareButton).addClass('disabled');
                $('.popup-compare').addClass('hidden')

            } else {
                $(this.compareButton).removeClass('disabled');

            }
        },
    });

    return BackendSelectRowCell;
});
