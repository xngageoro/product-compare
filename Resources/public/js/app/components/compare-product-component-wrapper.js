define(function (require) {
    'use strict';

    const BaseView = require('oroui/js/app/views/base/view');
    const mediator = require('oroui/js/mediator');
    const $ = require('jquery');
    const _ = require('underscore');
    const { productCompareInstance } = require('xngageproductcompare/js/app/models/product-compare-model')


    const CoompareProductWrapperComponent = BaseView.extend({


        /** @property */
        secModel: productCompareInstance,

        /**
         * @inheritDoc
         */
        constructor: function CoompareProductWrapperComponent(options) {
            CoompareProductWrapperComponent.__super__.constructor.call(this, options);
        },

        /**
         *
         * @param options
         */
        initialize: function (options) {

            //to get the products to compare when cookie expires    
            let ids = this.secModel.get('products').map(product => product.id);

            const value = `; ${document.cookie}`;

            let parts = value.split(`; compareProductArray=`);

            let arrayOfIds
            if (parts.length > 1) {
                arrayOfIds = JSON.parse(parts[1]?.split(";")[0]);
            } else {
                arrayOfIds = []
            }

            if (arrayOfIds.length !== ids.length) {
                document.cookie = `compareProductArray=${JSON.stringify(ids)};path=/`;

                setTimeout(() => {
                    mediator.trigger("updateCpmareProductCarousel")
                }, 1000)

            }

        },
    });

    return CoompareProductWrapperComponent;
});
