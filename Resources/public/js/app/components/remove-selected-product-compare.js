define(function (require) {
    'use strict';

    const BaseView = require('oroui/js/app/views/base/view');
    const mediator = require('oroui/js/mediator');
    const $ = require('jquery');
    const _ = require('underscore');


    const RemoveSelectedProductCompare = BaseView.extend({

        /** @property */
        id: null,

        /** @property */
        productListIndex: null,

        /**
         * @inheritDoc
         */
        constructor: function RemoveSelectedProductCompare(options) {
            RemoveSelectedProductCompare.__super__.constructor.call(this, options);
        },

        /**
         *
         * @param options
         */
        initialize: function (options) {
            if (options.id && options.dataRole) {
                $(`[data-role="${options.dataRole}"]`).on('click', this.handleRemoveSelectedClick.bind(this))
                this.id = options.id
            }
            this.productListIndex = options.productListIndex

            $(`[data-role="remove-all-products"]`).on('click', this.handleRemoveAllClick.bind(this))
            $(`[data-role="compare-products-back"]`).on('click', this.handleBackClick.bind(this))


        },

        handleRemoveSelectedClick: function () {
            mediator.trigger("removeSelectedCompare", this.id)
            mediator.trigger("updateCpmareProductCarousel")

        },
        handleBackClick: function () {
            let backLocation = JSON.parse(localStorage.getItem('productCompareBack'))

            if (backLocation) {
                window.location.assign(backLocation)
            } else {
                window.location.assign(this.productListIndex)
            }
        },
        handleRemoveAllClick: function () {
            
            mediator.trigger("removeallproducts")
            mediator.trigger("updateCpmareProductCarousel")

            this.handleBackClick()


        },
    });

    return RemoveSelectedProductCompare;
});
