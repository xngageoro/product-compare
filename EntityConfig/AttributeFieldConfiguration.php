<?php

namespace Xngage\Bundle\ProductCompareBundle\EntityConfig;

use Oro\Bundle\EntityConfigBundle\EntityConfig\FieldConfigInterface;
use Symfony\Component\Config\Definition\Builder\NodeBuilder;

/**
 * Provides validations field config for attribute scope.
 */
class AttributeFieldConfiguration implements FieldConfigInterface
{
    public function getSectionName(): string
    {
        return 'attribute';
    }

    public function configure(NodeBuilder $nodeBuilder): void
    {
        $nodeBuilder
            ->booleanNode('comparable')
                ->info('`boolean` controls whether attribute content can be comparable for in the storefront.')
                ->defaultFalse()
            ->end()
        ;
    }
}
