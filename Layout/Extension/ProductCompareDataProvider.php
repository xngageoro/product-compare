<?php
namespace Xngage\Bundle\ProductCompareBundle\Layout\Extension;

use Oro\Bundle\EntityConfigBundle\Manager\AttributeManager;
use Oro\Bundle\ProductBundle\Provider\ProductListBuilder;
use Symfony\Component\HttpFoundation\RequestStack;
use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\EntityConfigBundle\Entity\FieldConfigModel;


class ProductCompareDataProvider
{
    const PRODUCT_LIST_TYPE = 'compare_products';

    private RequestStack $request;
    private DoctrineHelper $doctrineHelper;
    private ProductListBuilder $productListBuilder;
    private AttributeManager $attributeManager;

    /**
     * @param DoctrineHelper $doctrineHelper
     * @param RequestStack $request
     */
    public function __construct(
        RequestStack $request,
        DoctrineHelper $doctrineHelper,
        ProductListBuilder $productListBuilder,
        AttributeManager $attributeManager
    ) {
        $this->request = $request;
        $this->doctrineHelper = $doctrineHelper;
        $this->productListBuilder = $productListBuilder;
        $this->attributeManager = $attributeManager;
    }

    public function getData(): array
    {
        $productsIds = json_decode($this->request->getCurrentRequest()->cookies->get('compareProductArray', "[]"));
        if (empty($productsIds)) {
            return [];
        }

        return $this->productListBuilder->getProductsByIds(self::PRODUCT_LIST_TYPE, $productsIds);
    }

    public function getProducts(): array
    {
        $productsIds = json_decode($this->request->getCurrentRequest()->cookies->get('compareProductArray', "[]"));
        if (empty($productsIds)) {
            return [];
        }

        return $this->doctrineHelper
            ->getEntityRepositoryForClass(Product::class)
            ->findBy(['id' => $productsIds]);
    }
    
    public function getProductAttributes(): array
    {
        return array_filter($this->attributeManager->getActiveAttributesByClass(Product::class), function($att) {
            return @$att->toArray('attribute')['comparable'];
        });
    }

    public function getProductAttributesLabel(): array
    {
        $labels = [];

        /** @var FieldConfigModel $attribute */
        foreach ($this->getProductAttributes() as $attribute) {
            $labels[$attribute->getId()] = $this->attributeManager->getAttributeLabel($attribute);
        }

        return $labels;
    }

}