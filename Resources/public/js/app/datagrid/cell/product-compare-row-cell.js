define(function (require) {
    'use strict';

    const $ = require('jquery');
    const _ = require('underscore');
    const __ = require('orotranslation/js/translator');
    const template = require('tpl-loader!xngageproductcompare/templates/datagrid/backend-select-product-compare-cell.html');
    const BaseView = require('oroui/js/app/views/base/view');
    const mediator = require('oroui/js/mediator');
    const messenger = require('oroui/js/messenger');
    const { productCompareInstance } = require('xngageproductcompare/js/app/models/product-compare-model')

    /**
     * Renders a checkbox for row selection.
     *
     * @class   oro.datagrid.cell.BaseView
     * @extends BaseView
     */
    const BackendSelectRowCell = BaseView.extend({
        /** @property */
        autoRender: true,

        /** @property */
        className: 'select-compare-cell renderable',

        /** @property */
        tagName: 'div',

        /** @property */
        template: template,

        /** @property */
        checkboxSelector: '[data-role="select-compare-cell"]',


        /** @property */
        secModel: productCompareInstance,

        /** @property */
        events: {
            'change :checkbox': 'onChange',
            // 'click [data-role="select-icon-dropdown-wrapper"]': 'handleDropdownIconClick'
        },
        /** @property */
        text: __('oro.product.grid.select_product'),

        /**
         * @inheritDoc
         */
        constructor: function BackendSelectRowCell(options) {
            BackendSelectRowCell.__super__.constructor.call(this, options);
        },

        /**
         * @inheritDoc
         */
        initialize: function (options) {

            if (options.productModel) {
                this.model = options.productModel;
            }
            
            this.$container = $(options._sourceElement);
            this.template = this.getTemplateFunction();

            mediator.subscribe('removeallproducts', this.onEventRemoveAllCalled, this);
        },

        /**
         * @inheritDoc
         */
        render: function () {
            const visibleState = {};
            let hide = !_.isMobile();
            let state = { selected: false }

            if (!_.isEmpty(visibleState)) {
                hide = visibleState.visible;
            }

            let ProdcutCompareArray = this.secModel.get('products');

            ProdcutCompareArray.forEach(element => {
                if (element.id === this.model.attributes.id) {
                    state.selected = true
                }
            })


            this.$el.html(this.template({
                checked: state.selected,
                text: this.text
            }));
            this.$checkbox = this.$el.find(this.checkboxSelector);
            this.$container.append(this.$el);
            this.hideView(hide);

            mediator.subscribe('addOrRemoveToPopupCompare', this.render, this);

            return this;
        },

        /**
         * @param {Boolean} bool
         */
        hideView: function () {
        },

        onEventRemoveAllCalled: function () {
            this.$el.html(this.template({
                checked: false,
                text: this.text
            }));
        },

        /**
         * When the checkbox's value changes, this method will trigger a Backbone
         * `backgrid:selected` event with a reference of the model and the
         * checkbox's `checked` value.
         */
        onChange: function (e) {
            const state = { selected: $(e.target).prop('checked') };
            let ProdcutCompareArray = this.secModel.get('products');

            let found = 0;
            ProdcutCompareArray = ProdcutCompareArray.filter(element => {
                if (element.id === this.model.attributes.id) {
                    found = 1;
                    return false
                } else {
                    return true
                }
            })
            if (!found) {
                ProdcutCompareArray = [...ProdcutCompareArray, {id:this.model.attributes.id,image:this.model.attributes.image,url:this.model.attributes.view_link}]
            }

            if (!ProdcutCompareArray.length) {
                $('.popup-compare').addClass('hidden')
            } else {
                $('.popup-compare').removeClass('hidden')
            }
            if (ProdcutCompareArray.length > 6) {
                messenger.notificationFlashMessage('warning', 'You have reached the maximum number of items (6).');
                this.render();
                return
            }
            mediator.publish('addOrRemoveToPopupCompare',ProdcutCompareArray)
        },
    });

    return BackendSelectRowCell;
});
