<?php
namespace Xngage\Bundle\ProductCompareBundle\Controller\Frontend;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Oro\Bundle\LayoutBundle\Annotation\Layout;
use Symfony\Component\HttpFoundation\Request;

class CompareController extends AbstractController
{
    /**
     * @Route("/", name="xngage_products_compare_index")
     * @Layout()
     *
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        return [];
    }
}