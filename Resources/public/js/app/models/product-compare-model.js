import _ from 'underscore';
import BaseModel from 'oroui/js/app/models/base/model';
import __ from 'orotranslation/js/translator';
import mediator from 'oroui/js/mediator';

const ProductCompareModel = BaseModel.extend({
    defaults: function () {
        return {
            products: []
        };
    },

    constructor: function ProductCompareModel(attributes, options) {
        ProductCompareModel.__super__.constructor.call(this, attributes, options);
    },

    initialize(attributes, options) {

        mediator.subscribe('removeallproducts', this.removeAll, this);
        mediator.subscribe('addOrRemoveToPopupCompare', this.removeAddItem, this);
        mediator.subscribe('removeSelectedCompare', this.removeItem, this);

        ProductCompareModel.__super__.initialize.call(this, attributes, options);

    },

    /**
     * Extends get methods to have getter functions for calculable attributes
     *
     * @param attr
     * @return {*}
     */
    get(attr) {
        if (typeof this[`get_${attr}`] == 'function') {
            return this[`get_${attr}`]();
        }

        return ProductCompareModel.__super__.get.call(this, attr);
    },

    removeAll() {
        this.set({ products: [] })
        localStorage.setItem('ProdcutCompareArray', JSON.stringify([]));
        document.cookie = `compareProductArray=${JSON.stringify([])};path=/`;
    },

    removeAddItem(products) {
        let ids = products.map(product => product.id)
        this.set({ products: products })
        localStorage.setItem('ProdcutCompareArray', JSON.stringify(products));
        document.cookie = `compareProductArray=${JSON.stringify(ids)};path=/`;
    },

    removeItem(id) {

        let products = this.get('products')
        products = products.filter(product => product.id != id)
        let ids = products.map(product => product.id)
        
        this.set({ products: products })
        localStorage.setItem('ProdcutCompareArray', JSON.stringify(products));
        document.cookie = `compareProductArray=${JSON.stringify(ids)};path=/`;
    },

    clear() {
        const { _order, ...defaults } = _.result(this, 'defaults');
        this.set(defaults);
    },

    isValidUnit: function () {
        return !this.get('units_loaded') || _.has(this.get('product_units'), this.get('unit'));
    }
});

export default ProductCompareModel;

const productCompareInstance = new ProductCompareModel({ products: JSON.parse(localStorage.getItem('ProdcutCompareArray')) || [] })
export { productCompareInstance };
